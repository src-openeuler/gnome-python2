Name:          gnome-python2
Version:       2.28.1
Release:       24
License:       LGPLv2+
Summary:       Extension module for Python
URL:           http://download.gnome.org/sources/gnome-python/
Source:        http://download.gnome.org/sources/gnome-python/2.28/gnome-python-%{version}.tar.bz2


Requires:      pygtk2 >= 2.10.3 libgnome >= 2.8.0 libgnomeui >= 2.8.0 gtk2 >= 2.6.0
Requires:      libgnomecanvas >= 2.8.0 libbonobo >= 2.8.0 libbonoboui >= 2.8.0
Requires:      pyorbit >= 2.0.1 GConf2 >= 2.11.1 gnome-vfs2 >= 2.14.0


BuildRequires: libbonobo-devel >= 2.8.0 libbonoboui-devel >= 2.8.0 libgnome-devel >= 2.8.0
BuildRequires: libgnomeui-devel >= 2.8.0 pygtk2-devel >= 2.10.3 pyorbit-devel >= 2.0.1
BuildRequires: python2-devel >= 2.3.0

Provides:      gnome-python2-gnome    = %{version}-%{release}
Provides:      gnome-python2-canvas   = %{version}-%{release}
Provides:      gnome-python2-bonobo   = %{version}-%{release}
Provides:      gnome-python2-gconf    = %{version}-%{release}
Provides:      gnome-python2-gnomevfs = %{version}-%{release}


Obsoletes:     gnome-python2-gnome    < %{version}-%{release}
Obsoletes:     gnome-python2-canvas   < %{version}-%{release}
Obsoletes:     gnome-python2-bonobo   < %{version}-%{release}
Obsoletes:     gnome-python2-gconf    < %{version}-%{release}
Obsoletes:     gnome-python2-gnomevfs < %{version}-%{release}
Obsoletes:     gnome-python2-nautilus <= 2.6.0

%description
This package is an extension module for Python.It includes the source packages for the Python
bindings for GNOME/libgnome/Canvas/Bonobo/GConf/gnome-vfs.

The package provides access to more widgets, a simple configuration interface,
and metadata support for providing access to basic GNOME libraries.

%package       devel
Summary:       Development files for gnome-python2
Requires:      gnome-python2 = %{version}-%{release} gnome-vfs2-devel >= 2.14.0
Requires:      python2-devel >= 2.3.0 pkgconfig

%description   devel
The devel package contains development files needed to build GNOME additional
library wrappers and interoperate with gnome-python2.

%prep
%autosetup -n gnome-python-%{version}

%build
%configure
%make_build

%install
%make_install
%delete_la
rm -rf examples/popt

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog NEWS
%doc %{_datadir}/gtk-doc/html/pygnomevfs
%dir %{python_sitearch}/gtk-2.0/gnome/
%dir %{_datadir}/pygtk/2.0/defs
%dir %{_datadir}/pygtk/2.0/argtypes
%dir %{python_sitearch}/gtk-2.0/bonobo/
%{python_sitearch}/gtk-2.0/*
%{_datadir}/pygtk/2.0/*
%{_libdir}/gnome-vfs-2.0/modules/libpythonmethod.so

%files devel
%defattr(-,root,root,-)
%{_includedir}/gnome-python-2.0
%{_libdir}/pkgconfig/gnome-python-2.0.pc
%defattr(644,root,root,755)
%doc examples/*

%changelog
* Wed Dec 11 2019 zhujunhao <zhujunhao5@huawei.com> 2.28.1-24
- Package init
